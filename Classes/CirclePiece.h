//
//  Token.h
//  Puzzler
//
//  Created by Daniel Bross on 19/05/2017.
//
//

#ifndef CirclePiece_h
#define CirclePiece_h

static int idCount;

using namespace cocos2d;
enum Direction
{
    None, Up, Down, Left, Right
};

enum Color
{
    Red, Blue, Purple
};

enum MovementMode
{
    NotMoving, Moving, HasMoved
};

class CirclePiece
{
public:
    CirclePiece(Vec2 position, Vec2 gridStart, Vec2 gridSize, Color color)
    {
        this->currentPosition = position;
        this->gridStart = gridStart;
        this->gridSize = gridSize;
        this->color = color;
        this->circleId = idCount++;
        
        sprite = Sprite::create("circle.png");
        sprite->setPosition(gridStart.x + position.x * 32, gridStart.y + (gridSize.y - position.y) * 32);
        
        switch(color)
        {
            case Red:
                sprite->setColor(Color3B(204, 50, 50));
                break;
            case Blue:
                sprite->setColor(Color3B(0, 178, 238));
                break;
            case Purple:
                sprite->setColor(Color3B(102, 102, 153));
                break;
        }
        
        Texture2D::TexParams texParams = { GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE };
        sprite->getTexture()->setTexParameters(texParams);
    }
    
    ~CirclePiece()
    {
        delete sprite;
    }

    inline Sprite* getSprite()
    {
        return sprite;
    }
    
    Vec2 getPosition()
    {
        return currentPosition;
    }
    
    void setPosition(Vec2 position)
    {
        sprite->setPosition(position);
    }
    
    void setPosition(float x, float y)
    {
        sprite->setPosition(x, y);
    }
    
    MovementMode getMovementMode()
    {
        return movementMode;
    }
    
    void setMovementMode(MovementMode mode)
    {
        movementMode = mode;
    }
    
    void goInDirection(Direction direction)
    {
        Vec2 position(0, 0);
        int distance = 0;
        
        switch(direction)
        {
            case Up:
                position = Vec2(currentPosition.x, 1);
                distance = std::abs(currentPosition.y - position.y);
                break;
            case Down:
                position = Vec2(currentPosition.x, gridSize.y);
                distance = std::abs(currentPosition.y - position.y);
                break;
            case Left:
                position = Vec2(0, currentPosition.y);
                distance = std::abs(currentPosition.x - position.x);
                break;
            case Right:
                position = Vec2(gridSize.x - 1, currentPosition.y);
                distance = std::abs(currentPosition.x - position.x);
                break;
        }
        
        if(distance == 0)
            distance = 1;
        
        FiniteTimeAction* action = MoveTo::create(0.2 * distance, Vec2(gridStart.x + position.x * 32, gridStart.y + (gridSize.y - position.y) * 32));
        CallFunc* completionHandler = CallFunc::create([this](){ setMovementMode(MovementMode::HasMoved); });
        sprite->runAction(Sequence::create(action, completionHandler, NULL));
        currentPosition = position;
        
        movementMode = MovementMode::Moving;
    }
    
    void pop() // @Incomplete: Create an animation for this!
    {
    }
    
    Color getColor()
    {
        return color;
    }
    
    
    int getId()
    {
        return circleId;
    }
private:
    int circleId;
    MovementMode movementMode;
    Sprite* sprite;
    Vec2 currentPosition;
    Vec2 gridStart;
    Vec2 gridSize;
    Color color;
};

#endif
